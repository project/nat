<?php

/**
 * @file
 * Provides a CTools (Panels) relationship that gets a node context from a term
 * created by NAT.
 */

/**
 * Implementation of specially named hook_ctools_relationships().
 */
function nat_nat_node_from_term_ctools_relationships() {
  $args['nat_node_from_term'] = array(
    'title' => t("Node from term (NAT)"),
    'keyword' => 'nat_node',
    'description' => t('Adds a NAT node from term context'),
    'required context' => new ctools_context_required(t('Term'), 'entity:taxonomy_term'),
    'context' => 'nat_nat_node_from_term_ctools_context',
  );
  
  return $args;
}

/**
 * Return a new context based on an existing context.
 */
function nat_nat_node_from_term_ctools_context($context, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_empty('node', NULL);
  }

  if (isset($context->data->tid)) {
    // Get the nid related to the term.
    $nid = key(nat_get_nids(array($context->data->tid)));

    if (!empty($nid)) {
      // Load the node.
      $node = node_load($nid);
      // Generate the context.
      return ctools_context_create('node', $node);
    }
  }
}
